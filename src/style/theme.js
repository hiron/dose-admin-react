import { createTheme } from "@mui/material";
import { red } from "@mui/material/colors";

const globalTheme = createTheme({
  palette: {
    background: {
      default: "#1e1c1c",
    },
    foreground: "#0d0e10",
    text: {
      primary: "#fff",
      secondary: "#fff",
      disabled: "#999999",
      hint: "#fff",
    },
    primary: {
      main: "#0d0e10",
      light: "#1e1c1c",
      dark: "#000000",
      contrastText: "#999999",
    },
    active: { color: "#ffc937" },
    secondary: {
      main: "#ffc937",
      //dark: "#f8542f",
      light: "#b3f663",
      contrastText: "#000000",
    },
    buttonSecondary: {
      main: "#b3f663",
      contrastText: "#000000",
    },
    bodyText: "#fff",
  },
  typography: {
    fontSize: 12,
  },
  MuiCssBaseline: {
    styleOverrides: {
      body: {
        scrollbarColor: "red green",
        "&::-webkit-scrollbar, & *::-webkit-scrollbar": {
          backgroundColor: "green",
        },
        "&::-webkit-scrollbar-thumb, & *::-webkit-scrollbar-thumb": {
          borderRadius: 8,
          backgroundColor: "red",
          minHeight: 24,
          border: "3px solid green",
        },
        "&::-webkit-scrollbar-thumb:focus, & *::-webkit-scrollbar-thumb:focus":
          {
            backgroundColor: "#959595",
          },
        "&::-webkit-scrollbar-thumb:active, & *::-webkit-scrollbar-thumb:active":
          {
            backgroundColor: "#959595",
          },
        "&::-webkit-scrollbar-thumb:hover, & *::-webkit-scrollbar-thumb:hover":
          {
            backgroundColor: "#959595",
          },
        "&::-webkit-scrollbar-corner, & *::-webkit-scrollbar-corner": {
          backgroundColor: "green",
        },
      },
    },
  },
});

export const theme = createTheme(
  {
    components: {
      MuiAutocomplete: {
        styleOverrides: {
          paper: {
            backgroundColor: globalTheme.palette.primary.main,
            marginTop: 10,
            marginLeft: -10,
            marginRight: -10,
          },
          clearIndicator: { display: "none" },
          popupIndicator: { display: "none" },
          inputRoot: { paddingRight: "0px !important" },
        },
      },
      Divider: {
        styleOverrides: { root: { border: "1px solid #ffffff !important" } },
      },

      MuiPaper: {
        styleOverrides: {
          root: {
            backgroundColor: globalTheme.palette.primary.main,
          },
        },
      },
    },
  },
  globalTheme
);
