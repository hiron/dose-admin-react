import { List, ListItemButton, ListItemIcon, ListItemText,Collapse  } from "@mui/material";
import { useSelector } from "react-redux";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import { NavLink } from "react-router-dom";
import { styled } from "@mui/system";
import {theme} from "../../style/theme";
import { useState } from "react";

export const MenuView = () => {
  const MENU = useSelector((state)=>state.menu);
  const [state, setState] = useState(
    MENU.reduce(
      (a, d) => ({ ...a, [d.menuName.replaceAll(" ", "_") + "State"]: false }),
      {}
    )
  );

  const SidebarButton = styled(ListItemButton)(({ theme }) => ({
    "&:hover": { backgroundColor: theme.palette.primary.dark },
  }));

  const menuController = (event) => {
    let key = event.target.offsetParent.id;
    setState({ ...state, [key]: !state[key] });
  };

  return(
  <List>
    {MENU.map((d) => {
      return (
        <>
          <SidebarButton
            onClick={menuController}
            id={d.menuName.replaceAll(" ", "_") + "State"}
          >
            <ListItemIcon>
              <i
                class="material-icons"
                style={{ color: theme.palette.secondary.main }}
              >
                {d.icon.toLowerCase()}
              </i>
            </ListItemIcon>
            <ListItemText primary={d.menuName} />
            {state[d.menuName.replaceAll(" ", "_") + "State"] ? (
              <ExpandLess />
            ) : (
              <ExpandMore />
            )}
          </SidebarButton>
          <Collapse
            in={state[d.menuName.replaceAll(" ", "_") + "State"]}
            timeout="auto"
            unmountOnExit
          >
            <List component="div" disablePadding>
              {d.SubMenus.map((f) => (
                <NavLink to={f.path}>
                  <SidebarButton sx={{ pl: 9 }}>
                    <ListItemText primary={f.subMenuName} />
                  </SidebarButton>
                </NavLink>
              ))}
            </List>
          </Collapse>
        </>
      );
    })}
  </List>);
};
