import { createSlice } from "@reduxjs/toolkit";

const MENU = [
    {
      id: 19,
      icon: "dashboard",
      menuName: "Menu",
      SubMenus: [
        {
          id: 35,
          path: "/dashboard",
          subMenuName: "Dashboard",
          MenuId: 19,
        },
      ],
    },
    {
      id: 20,
      icon: "store",
      menuName: "Agent",
      SubMenus: [
        {
          id: 36,
          path: "/agent",
          subMenuName: "Agent List",
          MenuId: 20,
        },
        {
          id: 37,
          path: "/request",
          subMenuName: "Agent Pending Request",
          MenuId: 20,
        },
      ],
    },
    {
      id: 21,
      icon: "payment",
      menuName: "Payment",
      SubMenus: [
        {
          id: 38,
          path: "/clear",
          subMenuName: "Clear Payment",
          MenuId: 21,
        },
        {
          id: 39,
          path: "/due",
          subMenuName: "Due Payment",
          MenuId: 21,
        },
        {
          id: 40,
          path: "/expired",
          subMenuName: "Expired Due",
          MenuId: 21,
        },
      ],
    },
    {
      id: 22,
      icon: "Content_Paste",
      menuName: "Order",
      SubMenus: [
        {
          id: 41,
          path: "/order",
          subMenuName: "Order List",
          MenuId: 22,
        },
        {
          id: 42,
          path: "/runing-order",
          subMenuName: "On-going Order",
          MenuId: 22,
        },
        {
          id: 43,
          path: "/history",
          subMenuName: "Order History",
          MenuId: 22,
        },
      ],
    },
    {
      id: 23,
      icon: "People",
      menuName: "Client",
      SubMenus: [
        {
          id: 44,
          path: "/client",
          subMenuName: "Client List",
          MenuId: 23,
        },
      ],
    },
    {
      id: 24,
      icon: "Vaccines",
      menuName: "Medicine",
      SubMenus: [
        {
          id: 45,
          path: "/medicine",
          subMenuName: "Medicine Management",
          MenuId: 24,
        },
      ],
    },
    {
      id: 25,
      icon: "Question_Answer",
      menuName: "FeedBack",
      SubMenus: [
        {
          id: 46,
          path: "/feedback",
          subMenuName: "Client Feedback",
          MenuId: 25,
        },
        {
          id: 47,
          path: "/faq",
          subMenuName: "FAQ",
          MenuId: 25,
        },
      ],
    },
    {
      id: 26,
      icon: "Discount",
      menuName: "Discount",
      SubMenus: [
        {
          id: 48,
          path: "/discount",
          subMenuName: "Discount List",
          MenuId: 26,
        },
      ],
    },
    {
      id: 27,
      icon: "Manage_Accounts",
      menuName: "User Mangement",
      SubMenus: [
        {
          id: 49,
          path: "/type",
          subMenuName: "User Type",
          MenuId: 27,
        },
        {
          id: 50,
          path: "/user",
          subMenuName: "Add User",
          MenuId: 27,
        },
      ],
    },
  ];

const menueSlice = createSlice({
    name: 'menue',
    initialState: MENU,
    reducers:{}
});

export default menueSlice.reducer;