import { Search as SearchIcon } from "@mui/icons-material";
import {
  Autocomplete,
  Box,
  InputAdornment,
  InputBase,
  styled,
} from "@mui/material";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

export const MenuSearchView = () => {
  const subMenus = useSelector((state) =>
    state.menu.reduce((a, d) => a.concat(...d.SubMenus), [])
  );
  const Search = styled("div")(({ theme }) => ({
    backgroundColor: theme.palette.primary.main,
    padding: "5px 10px",
    [theme.breakpoints.down("md")]: {
      width: "90%",
    },
    width: "40%",
    height: 40,
    borderRadius: theme.shape.borderRadius,
  }));
  
  return (
    <Autocomplete
      fullWidth
      options={subMenus}
      getOptionLabel={(option) => option.subMenuName}
      // classes={{ popper: { color: "red", fontWeight:"bold" } }}
      //popupIcon={<SearchIcon color="secondary" />}
      renderOption={(props, option) => {
        return (
          <Link to={option.path}>
            <Box component="li" {...props}>
              {option.subMenuName}
            </Box>
          </Link>
        );
      }}
      renderInput={({ InputLabelProps, InputProps, ...rest }) => {
        return (
          <Search>
            <InputBase
              autocomplete="off"
              placeholder="search..."
              {...InputProps}
              endAdornment={
                <InputAdornment position="end">
                  <SearchIcon color="secondary" />
                </InputAdornment>
              }
              {...rest}
            />
            {/* <SearchIcon style={{ cursor: "pointer", padding: "17px", color:"red" }} /> */}
          </Search>
        );
      }}
    />
  );
};
