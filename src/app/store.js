import {configureStore} from '@reduxjs/toolkit';
import menuRender from '../features/menu/menueSlice';

const store = configureStore({
  reducer: {
    menu: menuRender,
  },
});

export default store;