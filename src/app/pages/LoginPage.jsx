import { AccountCircleOutlined, Key } from "@mui/icons-material";
import {
  Stack,
  Button,
  Box,
  Grid,
  Container,
  TextField,
  InputLabel,
  InputAdornment,
  FormControlLabel,
  Checkbox,
  FormGroup,
} from "@mui/material";

import { useState } from "react";
import { useNavigate } from "react-router";


export const LoginPage = () => {
  const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [ckecked, setChecked] = useState(true);
  return (
    <Grid container>
      <Grid item xs={12}>
        <Stack
          color="#454545"
          m={{ sm: 3, xs: 2, lg: 7 }}
          justifyContent="center"
          alignItems="center"
        >
          <Box component="h1">Dose</Box>
        </Stack>
      </Grid>
      <Grid
        item
        xs={12}
        backgroundColor="foreground"
        sx={{ minHeight: "83vh", borderRadius: "50px 50px 0 0" }}
      >
        <Stack color="" justifyContent="center" alignItems="center" mt={10}>
          <Container maxWidth="sm" minWidth="xs">
            <InputLabel htmlFor="username" color="bodyText" mb={4}>
              <Box color="bodyText">User Name</Box>
            </InputLabel>
            <TextField
              fullWidth
              variant="outlined"
              size="small"
              id="username"
              value={username}
              onChange={(event) => {
                // console.log(event.target.value);
                setUsername(event.target.value);
              }}
              
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <AccountCircleOutlined color="secondary" />
                  </InputAdornment>
                ),
                style: {
                  backgroundColor: "#1e1c1c",
                  color: "#fff",
                  borderRadius: "7px",
                  marginBottom: "12px",
                },
              }}
            />

            <InputLabel htmlFor="password" color="bodyText" mb={4}>
              <Box color="bodyText">Password</Box>
            </InputLabel>
            <TextField
              type="password"
              variant="outlined"
              fullWidth
              size="small"
              id="password"
              value={password}
              onChange={(event) => {
                // console.log(event.target.value);
                setPassword(event.target.value);
              }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Key color="secondary" />
                  </InputAdornment>
                ),
                style: {
                  backgroundColor: "#1e1c1c",
                  color: "#fff",
                  borderRadius: "7px",
                  marginBottom: "12px",
                },
              }}
            />

            <FormControlLabel
              control={
                <Checkbox
                  checked={ckecked}
                  onClick={(event) => {
                    setChecked(event.target.checked);
                  }}
                  color="secondary"
                />
              }
              label="Keep me login"
            />

            <Box mt={2}>
              <Button
                variant="contained"
                color="secondary"
                size="small"
                fullWidth
                onClick={() => {
                  console.log("login is clicked");
                  //navigate("/dashboard", {replace: true});
                }}
              >
                Login
              </Button>
            </Box>
          </Container>
        </Stack>
      </Grid>
    </Grid>
  );
};
