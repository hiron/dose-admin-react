import { ArrowBack, ArrowUpward } from "@mui/icons-material";
import { Box, Button, Container, Stack, Typography } from "@mui/material";
import { Link } from "react-router-dom";

export const NotFound = () => {
  return (
    <Stack justifyContent="center" alignItems="center">
      <Container>
        <Typography
          sx={{ fontSize: "200px", fontWeight: "700" }}
          align="center"
        >
          404
        </Typography>
        <br />
        <Typography sx={{ fontSize: "30px", fontWeight: "200" }} align="center">
          Opps something went wrong!
          <br />
          <br />
          <Link to="/" replace>
            <Button variant="outlined" color="secondary">
              Go to Login <ArrowUpward />
            </Button>
          </Link>
        </Typography>
      </Container>
    </Stack>
  );
};
