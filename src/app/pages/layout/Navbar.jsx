import {
  Logout,
  Lock,
  NotificationsNone,
  PermIdentity,
} from "@mui/icons-material";
import {
  Autocomplete,
  Avatar,
  Badge,
  Divider,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  MenuList,
  Stack,
  styled,
} from "@mui/material";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { useState } from "react";
import { MenuSearchView } from "../../../features/menu/MenuSearchView";

import { theme } from "../../../style/theme";

export const Navbar = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    console.log(event);
    setAnchorEl(event);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const Icons = styled("Box")(({ theme }) => ({
    display: "flex",
    gap: "35px",
    itemAlign: "center",
  }));

  return (
    <AppBar
      position="sticky"
      elevation={0}
      sx={{ backgroundColor: theme.palette.background.default }}
    >
      <Toolbar display="flex" justifycontent="space-between">
        <MenuSearchView />
        <Icons>
          <Badge badgeContent={4} color="error">
            <NotificationsNone sx={{ width: 30, height: 30 }} />
          </Badge>
          <Avatar
            id="demo-positioned-button"
            sx={{ width: 35, height: 35 }}
            src=""
            onClick={handleClick}
          ></Avatar>
        </Icons>
      </Toolbar>
      <Menu
        color="secondary"
        id="demo-positioned-menu"
        aria-labelledby="demo-positioned-button"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        sx={{ marginTop: 5 }}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        {/* <MenuList> */}
          <MenuItem onClick={handleClose}>
            <ListItemIcon>
              <PermIdentity fontSize="medium" color="secondary" />
            </ListItemIcon>
            <ListItemText>Profile</ListItemText>
          </MenuItem>
          <MenuItem onClick={handleClose}>
            <ListItemIcon>
              <Lock fontSize="medium" color="secondary" />
            </ListItemIcon>
            <ListItemText>Change Password</ListItemText>
          </MenuItem>
          <Divider light={true}/>
          <MenuItem onClick={handleClose} sx={{ textTransform: "uppercase" }}>
            <ListItemIcon>
              <Logout fontSize="medium" color="secondary" />
            </ListItemIcon>
            <ListItemText>Logout</ListItemText>
          </MenuItem>
        {/* </MenuList> */}
      </Menu>
    </AppBar>
  );
};
