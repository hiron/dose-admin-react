import { Stack, Grid, Box } from "@mui/material";
import { Navbar } from "./Navbar";
import { Sidebar } from "./Sidebar";

export default function ({ children }) {
  return (
    <Grid container spacing={2}>
      <Grid
        item
        //backgroundColor="foreground"
        //sx={{ minHeight: "101.7vh", borderRadius: "0px 75px 0 0" }}
        display={{ xs: "none", sm: "block" }}
        sm={4}
        md={3}
        lg={2}
      >
        <Sidebar />
      </Grid>
      <Grid item xs={12} sm={8} md={9} lg={10}>
        <Stack spacing={2}>
          <Navbar />
          <Box
            p={3}
            pt={7}
            backgroundColor="foreground"
            sx={{ minHeight: "89vh", borderRadius: "75px 75px 0 0"}}
          >
            {children}
          </Box>
        </Stack>
      </Grid>
    </Grid>
  );
}
