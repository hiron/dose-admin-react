import { Box, Drawer, Grid, Stack, Toolbar } from "@mui/material";
import { useEffect, useRef, useState } from "react";

import { MenuView } from "../../../features/menu/MenuView";

export const Sidebar = () => {
  const drawerRef = useRef();
  const [width, setWidth] = useState(0);

  useEffect(() => setWidth(drawerRef.current.parentElement.offsetWidth - 10), []);
  return (
    <>
      <Drawer
        ref={drawerRef}
        variant="permanent"
        PaperProps={{
          sx: {
            boxSizing: "border-box",
            width: width,
            //position: "relative",
            borderRadius: "0px 75px 0 0",
          },
        }}
        sx={{
          display: { xs: "none", sm: "block" },
          //position: "relative",
        }}
      >
        <Toolbar>
          <Stack justifyContent="center" alignItems="center" m={3}>
            <Box component="h1" sx={{ fontWeight: 100 }}>
              <img
                src="../assets/img/Logo_white.png"
                width="130"
                alter="LOGO"
              ></img>
            </Box>
          </Stack>
        </Toolbar>
        <MenuView />
      </Drawer>
    </>
  );
};
