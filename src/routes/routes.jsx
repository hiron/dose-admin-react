import { Route, Routes } from "react-router";
import { Dashboard } from "../app/pages/Dashboard";
import { LoginPage } from "../app/pages/LoginPage";
import { NotFound } from "../app/pages/NotFound";

export default function () {
  return (
    <Routes>
      <Route path="/dashboard" element={<Dashboard />}></Route>
      <Route path="/" element={<LoginPage />}></Route>
      <Route path="*" element={<NotFound />}></Route>
    </Routes>
  );
}
