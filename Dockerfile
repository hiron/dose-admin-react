FROM node:18-alpine

RUN mkdir /dose-admin

WORKDIR /dose-admin

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY package.json /dose-admin/package.json
RUN npm install

ADD . .

ENTRYPOINT ["/entrypoint.sh"]

CMD ["npm", "run", "dev"]